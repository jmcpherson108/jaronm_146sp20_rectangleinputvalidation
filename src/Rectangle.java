/**
 *
 * @author jaronm
 * @version ICE for 24 Jan 2020
 */
public class Rectangle {
    
    /* FIELD(S) */
    private double length;
    private double width;
    
    /* CONSTRUCTOR(S) */
    public Rectangle( double length, double width)
    {
        this.length = length;
        this.width = width;
    } // end Rectangle 2-arg constructor

   
    /* METHOD(S) */

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }
    
    public double getArea ()
    {
        return length * width;
    } // end method getArea
    
    public double getPerimeter()
    {
        return length * 2 + width * 2;
    } // end method getPerimeter
            
} // end class Rectangle 
