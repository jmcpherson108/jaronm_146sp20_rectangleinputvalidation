
import java.util.Scanner;

/**
 * App to calculate the area and perimeter of a rectangle, given the length and
 * width entered by the user via standard input
 *
 * @author your_username@email.uscb.edu
 * @version ICE for 29 Jan 2020
 */
public class RectangleApp {

    public static void main(String[] args) {
        // display a welcome message
        System.out.println("Welcome to the Area and Perimeter Calculator");
        System.out.println();  // print a blank line

        // create the Scanner object
        Scanner sc = new Scanner(System.in);

        // perform invoice calculations until choice isn't equal to "y" or "Y"
        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            // get the number of rectangles from the user
            int numberOfRectangles = getIntWithinRange(
                    sc, "Enter number of rectangles to process (1 to 5): ", 1, 5);
            // instantiate an empty 1-D array of Rectangle
            // object references, with the length being
            // equal to numberOfRectangles
            Rectangle[] rectangleArray = new Rectangle[numberOfRectangles];
            
            // write a for loop that will prompt the user to specify
            // the length and width for each rectangle in your array
            // (and use that data to instantiate a Rectangle object, whose
            // reference will be assigned to the current element of the array)
            for (int i = 0; i < rectangleArray.length; i++) {

                System.out.println("For rectangle #" + (i + 1) + ":");

                double length = getDoubleWithinRange(
                        sc, "Enter length: ", 0, 1_000_000);
                double width = getDoubleWithinRange(
                        sc, "Enter width:  ", 0, 1_000_000);
                rectangleArray[i] = new Rectangle(length, width);

            } // end for

            // write a second for loop that will be used to prepare the
            // table of results
            System.out.println("Rect #\tLength\tWidth\tArea\tPerimeter");
            for (int i = 0; i < rectangleArray.length; i++) {

                System.out.print((i + 1) + "\t");
                System.out.print(rectangleArray[i].getLength() + "\t");
                System.out.print(rectangleArray[i].getWidth() + "\t");
                System.out.print(rectangleArray[i].getArea() + "\t");
                System.out.print(rectangleArray[i].getPerimeter() + "\n");
            } // end for

            // see if the user wants to continue
            choice = getChoiceString(sc, "Continue? (y/n): ", "y", "n");
            System.out.println();
        } // end while
    } // end method main

    /**
     * Method that forces the user to enter a double value within a specified
     * range
     *
     * @param sc
     * @param prompt
     * @param min
     * @param max
     * @return the validated double value
     */
    public static double getDoubleWithinRange(
            Scanner sc, String prompt, double min, double max) {
        double d = 0;
        boolean isValid = false;
        while (!isValid) {
            d = getDouble(sc, prompt);  // calls method that ONLY gets a double
            if (d <= min) {
                System.out.println("Error! Number must be greater than " + min);
            } else if (d >= max) {
                System.out.println("Error! Number must be less than" + max);
            } else {
                isValid = true;
            } // end multi-way if-else

        } // end while
        return d;

    } // end method getDoubleWithinRange

    public static int getIntWithinRange(
            Scanner sc, String prompt, int min, int max) {
        int i = 0;
        boolean isValid = false;
        while (!isValid) {
            i = getInt(sc, prompt);  // calls method that ONLY gets an int
            if (i < min) {
                System.out.println("Error! Number must be greater than or equal to "
                        + min + ".");
            } else if (i > max) {
                System.out.println("Error! Number must be less than or equal to"
                        + max + ".");
            } else {
                isValid = true;
            } // end multi-way if-else

        } // end while
        return i;

    } // end method getIntWithinRange

    // method that forces the user to enter a value of the double type
    // (NOTE: make a copy of this method and paste above, then edit accordingly)
    public static double getDouble(Scanner sc, String prompt) {
        double d = 0;
        boolean isValid = false;
        while (!isValid) {
            System.out.print(prompt);
            if (sc.hasNextDouble()) {
                d = sc.nextDouble();
                isValid = true;
            } else {
                System.out.println("Error! Invalid decimal value. Try again.");
            }
            sc.nextLine();  // discard any other data entered on the line
        }
        return d;
    } // end method getDouble

    // method that forces the user to enter a value of the int type
    public static int getInt(Scanner sc, String prompt) {
        int i = 0;
        boolean isValid = false;
        while (!isValid) {
            System.out.print(prompt);
            if (sc.hasNextInt()) {
                i = sc.nextInt();
                isValid = true;
            } else {
                System.out.println("Error! Invalid value. Try again.");
            } // end if/else
            sc.nextLine();  // discard any other data entered on the line
        } // end while
        return i;
    } // end method getInt

    public static String getChoiceString(
            Scanner sc, String prompt, String s1, String s2) {
        String s = "";
        boolean isValid = false;
        while (!isValid) {
            s = getRequiredString(sc, prompt);
            if (!s.equalsIgnoreCase(s1) && !s.equalsIgnoreCase(s2)) {
                System.out.println("Error! Entry must be '"
                        + s1 + "' or '" + s2 + "'. Try again.");
            } else {
                isValid = true;
            } // end if/else
        } // end while
        return s;
    } // end method getChoiceString

    // method that forces the user to enter a complete string (a line of text)
    // and not just a blank line
    // (NOTE: make a copy of this method and paste above, then edit accordingly)
    public static String getRequiredString(Scanner sc, String prompt) {
        String s = "";
        boolean isValid = false;
        while (!isValid) {
            System.out.print(prompt);
            s = sc.nextLine(); // this reads a whole line (not just a token)
            if (s.equals("")) {
                System.out.println("Error! This entry is required. Try again.");
            } else {
                isValid = true;
            }
        } // end while
        return s;
    } // end method getRequiredString

} // end class RectangleApp
