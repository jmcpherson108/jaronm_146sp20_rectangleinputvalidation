/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
// 
import org.junit.Assert;
// import static org.junit.Assert.*;

/**
 *
 * @author jaronm
 */
public class RectangleTest {
    
    private final Rectangle rectangleInstance = new Rectangle(3.0, 3.0);
    
    /**
     * If you write a Javadoc comment for a method that 
     * uses an "@" annotation, make sure the comment 
     * comes BEFORE the annotation.
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getArea method, of class Rectangle.
     */
    @Test
    public void testGetArea() {
        System.out.println("getArea");
        double expectedResult = 9.0; // 3.0 * 3.0
        double testResult = rectangleInstance.getArea();
        Assert.assertEquals(expectedResult, testResult, 0.00005);
    }

    /**
     * Test of getPerimeter method, of class Rectangle.
     */
    @Test
    public void testGetPerimeter() {
        System.out.println("getPerimeter");
        double expectedResult = 12.0; // 2*3.0 + 2*3.0
        double testResult = rectangleInstance.getPerimeter();
        Assert.assertEquals(expectedResult, testResult, 0.00005);
    }
    
}
